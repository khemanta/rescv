%%%%-------------------- Resume Author, Kumar Hemant ------------------------
\documentclass[margin,line,a4paper,10.5pt]{res}
\usepackage{graphicx}
\usepackage{graphics}
\usepackage{float,epsfig,floatflt,here}
\usepackage{color}
\usepackage{pgf}
\usepackage[AGaramondPro]{mathdesign}
%%---------------------------- setting fonts --------------------------------
\usepackage{fontspec}
\setmainfont{TeX Gyre Pagella}  %{Garamond, TeX Gyre Pagella}
%\setmainfont[Ligatures=TeX]{TeX Gyre Pagella}%{TeX Gyre Chorus}%{TeX Gyre Pagella}
%%Times, Times New Roman, serif;  Alegreya, AGaramondPro-Regular, AGaramondPro-Semibold, GaramondBE, Apple Garamond, Arial, Calibri, Consolas, Constantia
%% Times New Roman / Serif fonts: Georgia, Bell MT, Goudy Old Style, Garamond / Sans serif : Arial, Tahoma, Century Gothic, Lucida Sans,
%\setmainfont[C:/kumarh/LaTeX/XeTeX/Fonts]{Ubuntu}
%%\newcommand\customfont[1]{{\usefont{T1}{custom}{m}{n} #1 }}
%%\customfont{AGaramondPro-Regular}
%%----------------------------------------------------------------------------

%\usepackage{tfrupee}
\setlength{\textheight}{9.2in} % increase text height to fit on 1-page 9.9
\oddsidemargin -0.750in
\evensidemargin -0.750in
\textwidth=6.0in % original 6.0in
\itemsep=-0.0in %0.10in
\parsep=-0.0in %-0.10in

%---------------------------page counting, header/footer----------------------
\usepackage{fancyhdr}
%\usepackage{lastpage}

\pagestyle{fancy}
\fancyhf{} % sets both header and footer to nothing
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
%\fancyhead[LE,RO]{\slshape \rightmark}
\fancyfoot[LE,RO]{\slshape hemant,k.}
\fancyfoot[RE,LO]{\slshape \LaTeXe resume}
\fancyfoot[C]{\thepage}
%\headrulewidth 0.0pt
%\footrulewidth 0.4pt
%%--------------------------- fancy header or footer -------------------------
%\thispagestyle{empty}
\thispagestyle{fancy}
\newenvironment{list1}{
  \begin{list}{\ding{113}}{%
      \setlength{\itemsep}{0in}
      \setlength{\parsep}{0in} \setlength{\parskip}{0in}
      \setlength{\topsep}{0in} \setlength{\partopsep}{0in}
      \setlength{\leftmargin}{0.05in}}}{\end{list}}
\newenvironment{list2}{
  \begin{list}{$\bullet$}{%
      \setlength{\itemsep}{0in}
      \setlength{\parsep}{0in} \setlength{\parskip}{0in}
      \setlength{\topsep}{0in} \setlength{\partopsep}{0in}
      \setlength{\leftmargin}{0.05in}}}{\end{list}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
%\name{{\huge \bf Kumar Hemant} \vspace*{0.10in}} %0.10in

%\vfill
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{-3.5in}
\begin{figure}%[!h]
%%\centering
\flushright%right
%\hspace{-3.0in}
%\psfig{file=CurriculumViate-Heading,width=\textwidth} %
\psfig{file=hem.png,width=.11\linewidth}
%%\hfill Kumar Hemant
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{resume}
\section{\sc Contact\\ Information}
%\vspace{.05in}
\begin{tabular}{@{}p{3.3in}p{4in}}
%------------------------------------------------------------------------------------------------------------------------------------
Edgedale Plains   & {\it Cell: } (+65).94.577.206                           \\ %{\it Voice:}  (+91).80.410.59203 E.59203 \\
Blk 135, \#16--84 & {\it Email: }{\small kumar.hemant.iitg@gmail.com}       \\ %{\it Cell:}   (+91).99729.19547  \\
820135 Singapore  & {\it W3: }   {\small http://sg.linkedin.com/in/khemant} \\ %{\it E-mail:} {\tt hemant\_iitg@hotmail.com}\\
%Singapore        & {\it WWW:} www.linkedin.com/in/khemant/                 \\ %
%                 & {\it WWW:} www.googlepages.com/\verb+~+hemant           \\ %
%%------------------------------------------------------------------------------------------------------------------------------------
%%------------------------------------------------------------------------------------------------------------------------------------
\end{tabular}

\section{\sc Summary}
%{ \it MS in Applied Mathematics \& Computer Science from IITG with around 8 years of experience in Data Science, Software Engineering, R\&D, IT/Consulting with focus on Data-Mining (Supervised, Semi-supervised \& Unsupervised), Machine Learning, Large Scale Scientific Computing, Business Analytics, Mathematical and Statistical Modeling.}

% $\sim$ or $ \textasciitilde $ or $ \texttildelow $ or $ \textbackslash $

{\it MS in Applied Mathematics \& Computer Science from IIT/G with around 8 years of experience in Analytical Consulting and R\&D with focus on Data-Mining, Machine Learning, Statistical Modeling in supporting fortune 500 clients in consulting engagements in B2B space. Enriched experience in implementing various statistical techniques (Linear Regression, Logistic Regression, Market Mixed Models, Survival Analysis, Customer Loyalty Analysis). Experience in financial score card model building for customer attrition, cross-sell and up-sell models and complex strategic recommendations.}

%{\small Exposure in domains like online search \& display advertisement, Retail, Airlines \& Aircraft manufacturing and basics of financial concepts. Currently working on reliability statistics and engineering analysis to optimize the aircraft maintenance time intervals using the lifetime distributions.}

\vspace{.10cm}

\section{\sc Education}
%{\bf National University of Singapore, Singapore}\\
%%%{\large \sf Nanyang Technological University, Singapore}\\
%{\em Institute of Systems Science}\\
%\vspace*{-.15in}
%\begin{list1}
%\item[] {M.Tech.} in Knowledge Engineering,  Jan/14 - Jul/15
%%%\item[] {M.Eng.} in Software Engineering,  June 2012
%\end{list1}
%
{\bf Indian Institute of Technology} Guwahati \hfill India\\
\vspace*{-.15in}
\begin{list1}
%\item[]{\it M.S. in Mathematics \& Computing} \hfill {June 2005}
\item[]{M.Sc.} in Mathematics \& Computing [{\sc cgpa 5.72}] \hfill Jul/03 - Jun/05
\end{list1}
\vspace*{-.05in}
{\bf The Banaras Hindu University} Varanasi \hfill India\\
\vspace*{-.15in}
\begin{list1}
%\item[]{\it B.S. (Honors) in Physics} \hfill {April 2003}
\item[]{B.Sc.}, Major  Physics, Minor Math/Stats [{\sc aggt 55\%}] \hfill Jul/99 - Apr/03
\end{list1}

%\section{\sc Professional\\ Trainings}
%{\bf Indian Institiute of Management Lucknow, India \&} \\
%{\bf Kelly School of Business at Indiana University--Bloomington, US} \\
%%{\bf North Carolina State University}, NC \\
%%{\em Institute of Advance Analytics}
%\vspace*{-.15in}
%\begin{list1}
% \item[] Certificate Programme in Business Analytics for Executives (CPBAE)
% %\item[] Master of Science in Analytics, December 2008 (expected graduation date: May 2011)
%\end{list1}
%%{\bf National University of Singapore}, Singapore \\
%%%{\em Department of Statistics}
%%\vspace*{-.15in}
%%\begin{list2}
%%\item[] M.Tech, Software Engineering, December 2010 (expected
%%  graduation date: Dec 2011)
%%\end{list2}

%\vspace{.5cm}

\section{\sc Computing\\Skills}
\begin{list2}
\item Languages: C/C++, R, SAS, SQL, {\sc Java, Matlab}, MPI
\item Scripting: Python, Unix-shells, JavaScript
\item O/S Platform: Unix, Linux(SuSE, Fedora, Ubuntu), Solaris, WinXP/7
\item Database Platform: MySQL, PostgreSQL, SciDB, Informix, Oracle 9i
\item Applications \& Text Processing: \LaTeXe, MS Office, OpenOffice.Org
%\item Applications \& Text Processing: \LaTeXe, database, spreadsheet, and presentation software
%\item Mathematical \& Statistical Packages: Notion of SAS/STAT, A bit of R \& Matlab
\item Data Mining \& Machine Learning: SPSS Modeler, R, Weka, Mahout, Hadoop, $etc.$ \\
%\item Algorithms \& Analysis: Mathematical Modeling and Computer Simulations \\
\end{list2}

%\section{\sc Relevant\\Courses}
%{\bf Applied Mathematics :} {\it Numerical Analysis \& Scientific Computing, Mathematical Modeling \& Numerical Simulation, Numerical Linear Algebra, Computational Fluid Dynamics, Optimization Techniques. } {\bf Computer Science :} {\it Programming in C, Data-Structure, Advance Algorithms, Graph Theory \& Combinatorics, Formal Languages \& Automata Theory, Theory of Computation etc.}

\section{\sc Professional\\ Experience} %{\sc Professional Experience}
%
{\bf IBM Global Services - Singapore} \hfill SINGAPORE

\vspace{-.3cm}
{\it Associate Architect} \hfill {\it January, 2013 - Present}\\
Associate Architect in advance analytics at IBM Center of Competency (CoC) in Smarter Supply Chain Analytics, is part of IBM Global Services - Singapore.

%IBM Research Collaboratory - Singapore, part of IBM Singapore Pte Ltd.\\
{\bf IBM Research - Singapore} \hfill  SINGAPORE

\vspace{-.3cm}
{\it Research - Staff Software Engineer} \hfill {\it September, 2011 - December 2012}\\
IBM Singapore Research Collaboratory part of IBM Research, the R\&D unit of {\sc IBM}, Singapore\\
{$\diamond$} Worked in Business Analytics \& Mathematical Science unit for smarter planet initiatives mainly large scale analytical projects. Using multi-modal data system from multiple sources (Science Data) and design python API specification to use and query with low latency in application like near realtime decision making process and business analytics using statistical and data-mining techniques.


{\bf Infosys Technologies Ltd} \hfill Bangalore, INDIA

\vspace{-.3cm}
{\it Junior Research Associate / Member Research Staff} \hfill {\it May, 2010 - August, 2011}\\
Software Engineering \& Technology Labs (InfosysLabs), the R\&D arm of {Infosys Technologies Ltd}, Bangalore, India.\\
{$\diamond$} Worked with product development team (HIMI) in capacity of Subject Matter Expert (SME) for development of an analytical model development workbench for configuring and customizing statistical and machine learning based model for telecom, banking and retail domain. The statistical techniques were regression (linear, logistics), supervised techniques (classification), un-supervised  techniques (clustering) and recommendation systems (collaborative filtering) etc.

{\bf Infosys Technologies Ltd} \hfill Bangalore, INDIA

\vspace{-.3cm}
{\it Analyst - Specialist} \hfill {\it February, 2009 - April, 2010}\\
On tenure-track secondment to SETLabs from Knowledge Services. SETLabs, the R\&D unit of {Infosys Technologies Ltd}, Bangalore, India.\\
{$\diamond$} Worked in the area of customer analytics. Research aspect area includes, NLP, text mining and machine learning techniques applicable in various domain using unstructured and structured data for a flagship product called Voice of Customer (VoC). Consulting aspect of work involves ad hoc request for proposal and consultation for RFP, product landscaping, patent analysis. Liasoning between research team and product engineering team for integration of ideas/IPs for go-to-market. %Worked with other product team iCAT (Infosys Category Analytics) for integrating the iCAT platform with supply chain analytic suite of capabilities. Worked on RFP and PoC for major US reatiler HomeDepot. Disclosed a patent on the project.

{\bf Infosys Technologies Ltd} \hfill Bangalore, INDIA

\vspace{-.3cm}
{\it Analyst - Programmer} \hfill {\it August, 2007 - January, 2009}\\
At Knowledge Services Analytics Unit, integrated part of Infosys Consulting, Inc; a wholly owned subsidiary of {Infosys Technologies Ltd}., Bangalore, India.

{$\diamond$} {\emph Client:} {\bf Microsoft Corporation} Worked with Microsoft's adCenter team being part of Infosys {\it Analytics Center of Excellence (ACoE)} in capacity of SAS programmer, Statistical Modeler and SME particularly in web-analytics area for MSN search and MSN Live (bing). Developed generic codes to crunch the data using data-mining techniques to offer analytical insight and consultancy in web-analytics and online search \& marketing. Made predictive and statistical model to help optimize the MSN ad placing, recommending automatic bidding strategy based on Dutch, English, GFP and GSP auction system.  Data used was search keywords mapped search internal traffic data and third party (Omniture, ComScore).
%1.	{\bf Search \& Churn:} To figure out key customer (advertisers) who contribute to the largest basket of revenue using lift curve and pareto-principle. And pro-active retention of those who are prone to churn using churn model.\\
%2.	{\bf Capacity Forecast:} Optimal forecast of inventory (impressions) for advertiser at various page group level to reduce the under booking and over booking situation. Also to identify bidding strategy for the remnant and reserved ad space using automatic-bidding strategy based on Dutch and English auction system.\\
%3.	{\bf MSN Direct Response:} This project was to know using capacity forecast model, the opportunity cost of under-booking and over-booking along with performance of auction system of remnant ad space.\\
%4.	{\bf PPC Analysis:} Pay per click analysis was to look at the PPC as KPI over CPM, and to tap any potential fraud in clicks by ad-networks.

{\bf Startup: in pursuit of entrepreneurial interests} \hfill Delhi/Pune, INDIA
%{\bf Infinite Techpark Pvt Ltd} \hfill Delhi/Pune, INDIA

\vspace{-.3cm}
{\it Technology Evangelist/Independent Consultant} \hfill {\it June, 2006 - July, 2007} \\
Worked in entrepreneurial interest for web based student email service, development of e-learning system (K12) and cloud based educational docket service. Evangelized and bootstrapped the idea while reaching out for angel investors. % and in-house product call 'Think-T' for customized web based email hosting solution and spam guard. The product is based on LAMP configuration (Linux, Apache, MySQL and PHP). The open source code of Fedora Core 5 was taken into consideration while designing the product.

{\bf Forschungszentrum J\"ulich GmbH} \hfill Juelich, GERMANY

\vspace{-.3cm}
{\it Wissenschaftlicher-Mitarbeiter/Young Scientist} \hfill {\it October, 2005 - May, 2006}\\
Worked as young scientist cum scientific programmer on IBM Bluegen supercomputer cluster at J\"ulich Supercomputing Center of Helmholtz Research Center Juelich GmbH, Germany. % on serial and parallel open source code of Fire Dynamics in Computational Fluid Dynamics (CFD).

\vspace{-0.15cm}
1.	Worked on open source code of FDS in the area of CFD in serial and parallel environment. Wrote executables for data processing and analysis using C, MPI routines and UNIX Shell-scripting. \\
%Wrote a technical paper "FDS: A User Interface for Simulation of FDS Version 4.0" for Unix/Linux platform. Designed input files for simulation of FDS (Fire Dynamics simulator). Performed simulations and analysis of the results for different run and test cases of simulation for serial and parallel environment.
%Pedestrian dynamics and simulation of traffic flow of human behavior during stampede. simulation of pedestrian movement and dynamic lane formation.
2.	Participated and collaborated with Fire and Safety engineering group of University of Wuppertal. Part of work was funded by DFG for traffic simulation in stadia/arena to test the safety measure of the stadium in likelihood of a stampede for the FIFA World Cup in Germany in Year 2006.

\section{\sc Internship}
\vspace{-.10cm}
%\begin{itemize}
{\bf ISRO-IITK Space Technology Cell} \hfill IIT Kanpur, INDIA. \\
{\it Research Intern} \hfill {\it August, 2005 - September, 2005}\\
$\diamond$ {\bf Computer Simulation of a Gas Discharge in an EM field.} Worked on plasma physics modeling of jet thrust gaseous discharge. Used open source code (ES1) for 'Particle in Cell' model for simulations of 1-D Vlasov problem. \hfill {\bf Supervisor:} Dr. Phalguni Gupta/Dr. Nandini Gupta
%\end{itemize}

\section{\sc Accolades}
{\it
$\diamond$ DAAD-Helmholtz young scientist studium (fellowship), 2005. \\
%German Research Foundation (DFG) research studium (fellowship), 2006 \\
$\diamond$ Secured highest percentile in a national level GATE'05 (Engineering Science) in IITG zone.\\
%Merit-cum-means scholarship at IIT Guwahati during master course work \\
% Studium (fellowship) for research at Helmholtz Research Center Juelich, Germany. \\
% Qualified national level Graduate Aptitude Test in Engineering in Engineering Science (XE) with highest percentile in IIT-G zone.\\
$\diamond$ Won in `On the Spot Programming Contest' in Techniche 2005 at IIT Guwahati.
 }

\section{\sc Technical/ \\Research\\Papers}
\begin{enumerate}
\item Kumar Hemant, Vikas Gupta, Abhijit Choudhary (Dec 2004): {\it ``Numerical Solution of 2D Navier-Stokes Equation for Incompressible Viscous Lid-Driven Cavity Flow"}, Department of Mathematics, IIT Guwahati.
\item Manish Mishra, Chandan Pandey, Kumar Hemant (Jul 2005): {\it ``Assembly of Stiffness Matrix in Shared Memory Environment"}, Department of Mechanical Engineering, IIT Guwahati.
\item Kumar Hemant, Bernd Koerfgen, Armin Seyfried, (Dec 2005): {\it ``FDS: A User Interface for Simulation on Unix/Linux Platform"}, ZAM, Forschungszentrum Juelich, Germany.
\item Armin Seyfried, Kumar Hemant, et al, (April 2006): {\it``Simulation of Pedestrian Dynamics with Bottle-Neck Effect"}, ZAM, Research Center Juelich, Germany
\item Kumar Hemant, Petra Jansen, Dora Erd\"os, et al.,(Apr 2009): {\it ``Optimization routine for hot rolling mill"}, IJmuiden Research Lab, Corus Netherlands B.V., The Netherlands.
\end{enumerate}

\section{\sc  Workshops/ Conferences}
\begin{enumerate}
\item Mathematical Training \& Talent Search (MTTS) held at IIT Guwahati (01st - 14th Dec, 2004)
\item NBHM and INSA Summer School on Stochastic Process \& Applications held at IIT Guwahati (11th - 23rd July, 2005)
\item Mathematical Modeling Week at Technical University of Eindhoven, The Netherlands (28th March 2008 - 11th April 2008)
\end{enumerate}

\section{\sc Patents}
\begin{itemize}
\item {\sc System and method for personalized contextual ad recommendation on a temporal framework across multiple screens of content delivery.} ({\it USPTO\# 2012/0078,725})
\item {\it Method and Systems for online auction systems for stability against gaming-the-system.} -- {\it disclosed }
\item {\it Optimizing the inventory and replenishment with Real-Time-Information flow  using the SKUs code.} -- {\it disclosed}
\end{itemize}

%\section{\sc Professional\\ Trainings @} College Of Engineering Pune (COEP) \verb"<www.coep.org.in>"
%\begin{itemize}
%  \item Certificate course in Linux \& Networking at COEP, Pune.
%  \item Certificate course in Software Testing from COEP, Pune.
%%  \item Certified Software Quality Test Analyst from Computer Society of India, Chennai.
%%  \item Certified Base SAS Programmer from SAS Institute, Inc., North-Carolina, USA.
%  \item Infosys internal trainings and certifications on SAS, SQL, Python, Java, etc.
%\end{itemize}

%\section{\sc Relevant\\Courses}
%{\bf Applied Mathematics : } {\it Numerical Analysis \& Scientific Computing, Mathematical Modeling \& Numerical Simulation, Numerical Linear Algebra, Computational Fluid Dynamics, Optimization Techniques} {\bf Computer Science : } {\it Programming in C, Data-Structure, Algorithms, Graph Theory \& Combinatorics, Formal Languages \& Automata Theory etc.}

\section{\sc Cocurricular\\Activities}
\begin{enumerate}
\item Secretary, Mountaineering \& Trekking Club at Gymkhana Council, IIT Guwahati.
\item Certificate in Photography from Hobby Center, BHU Varanasi.
\item Certificate in Mountaineer training course from NIM, Uttarkashi.
\item Climbed a peak in the Himalayas as rope leader in annual mountaineering expedition year 2001 in the vicinity of Adi Kailash (\~ 18000ft), Pithoragarh in Uttaranchal.
\item Represented Faculty of Science in the university level events of Quiz, Debate \& GD in the Inter-Faculty Youth Festival held at IT-BHU in year 2003.
%\item Organizing member of AKANKSHA, a university level cultural festival of BHU Varanasi.
\item Served as student secy. for BHU Chapter of Indian Association of Physics Teachers.
\item Attended a Diploma in French course at Dept of French Studies, BHU Varanasi.
\item Attended basic course in German language at Gastehaus Forschungszentrum Juelich, Germany.
\end{enumerate}

%\section{\sc Personal\\Details}
%\vspace{-0.2cm}
%\begin{tabbing}
%%1234567890123 \= 12345678901234567890123456\= 123456789012345678901234567 \= 123456789012345678901234567  \kill
%1234567890123456 \= 1234567\= \kill
%Father's Name   \> : \> Late Sri D. N. Mishra \\
%DOB             \> : \> August 15, 1977 \\
%Nationality     \> : \> Indian \\
%Marital Status  \> : \> Married \\
%Passport        \> : \> F3211761   \\
%Visa Status     \> : \> B1/B2 (Multiple entry business visa for US)\\
%Current CTC     \> : \> SG\$ 75,400/annum \\ % 30,50,000.00/annum (SG\$ 6878.00/month) \\
%\end{tabbing}

\section{\sc Additional Informations}
\vspace{-0.10cm}
\begin{tabbing}
12345678901234567\= 123 \= \kill
Current CTC      \>:\> SG\$89,700/annum + 12\% annual salary (Bonus) \\ % 30,50,000.00/annum (SG\$ 6878.00/month) \\
Expected CTC     \>:\> SG\$150,000/annum \\ %90,000/annum (negotiable)\\
Notice Period    \>:\> 6-8 weeks \\
Total Exp.       \>:\> 8.0+ Years \\
Relevant Exp.    \>:\> 6.0+ Years \\
Current Location \>:\> Singapore \\
Relocation       \>:\> Open to relocation US/UK/EU/ANZ opportunities.\\
    %	         \>:\> and possible onsite US/UK/EU/ANZ opportunities.\\
    		     \>:\> Open to India based rewarding opportunities or startups.
%%Personal Photo \>:\>
\end{tabbing}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{figure}[!h]
%\vspace{-8.2cm}
%%\centering
%\flushright
%\psfig{file=147522.eps,width=.15\linewidth}
%%\hfill KumarHemant
%\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{\sc Professional Certifications}
%\vspace{-0.05cm}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{figure}[!h]
%%\centering
%%\leftalign
%\psfig{file=SAStm.eps,width=.20\linewidth}
%\psfig{file=OCA.eps,width=.25\linewidth
%%\psfig{file=OCP.eps,width=.225\linewidth}
%\psfig{file=JAVAtm.eps,width=.10\linewidth}
%\psfig{file=ISTQB.eps,width=.22\linewidth}
%%\hspace{0.5cm}
%\psfig{file=CSI.eps,width=.13\linewidth}
%%\vspace{-0.5cm}
%%\psfig{file=swtISTQB.eps,width=.20\linewidth}
%%\psfig{file=basesas.eps,width=.30\linewidth}
%%\caption{SAS Certified Base Programmer}
%\label{SAS Certified Base Programmer}
%\end{figure}
%\vspace{-0.2cm}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{tabbing}
%123456789012345678 \= 12345678901234567890  \= 123456789012345  \= 123456789012345789  \=      \kill
%SAS Certified   \> ORACLE  Certified    \> SUN Certified    \> ISTQB Certified  \> CSI \\
%Base Programmer \> PL/SQL  Developer    \> Java Associate   \> Software Tester  \>  of India       \\
%\end{tabbing}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{tabular}%{|c|c|c|c|c|}
%{|l|l|l|l|l|}
%%\hline
%SAS certified   & ORACLE certified  & SUN Certified  & ISTQB certified & CSI's Software  \\
%Base Programmer & PL/SQL Associate  & Java Associate & Software Tester & Quality Test Analyst\\
%%\hline
%\end{tabular}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{tabular}{|c|c|c|c|c|}
%  \hline
%  % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
%   &  &  &  &  \\
%  \hline
%\end{tabular}

%\newpage{}*
%

\section{\sc References}
Available on request.
% \vspace{-0.10in}
%% \hspace{-1.0in}
%\begin{small}
%\begin{tabbing}
%%%   12345678901234567890123456789 \= 12345678901234567890123456789 \= \kill
%\hspace{4.5cm}                    \=      \hspace{5.5cm}           \=     \hspace{5cm}          \kill
%%%% ----------------------------------------  Reference ----------------------------------------------
%%%   {\bf Dr Sanjoy Paul}         \> {\bf Dr Chinghua Chen-Ritzo}  \>  {\bf Mr Suvendu Tripathy} \\
%%%   SVP, Head Global Research    \>  Research Director		       \>  Founder \& CEO            \\
%%%   Accenture Technology Lab     \>  IBM Research Collaboratory   \>  Mouse-e Retail Pvt Ltd    \\
%%%   Bangalore, India    		  \>  IBM Singapore Pte Ltd        \>  Bangalore, India          \\
%%%   sanjoy.paul@accenture.com    \>  chinghua@sg.ibm.com 		   \>  suvendu@mouse-e.com       \\
%
%%%% ------------------------------------ Academic Reference ------------------------------------------
%%%   {\bf Dr R\"udiger Esser}        \>  {\bf Dr Purandar Bhaduri}  \>  {\bf Dr D C Dalal}          \\
%%%   Dy Director, JSC                \>   Associate Professor       \>  HOD \& Associate Professor  \\
%%%   J\"ulich Supercomputing Center  \>   Dept of Computer Science  \>  Department of Mathematics   \\
%%%   RWTH Aachen, Germany            \>   IIT Guwahati, India       \>  IIT Guwahati, India         \\
%%%   %D-52425, NRW, Germany          \>                             \>                              \\
%%%   r.esser@fz-juelich.de           \>   p.bhaduri@iitg.ac.in      \>  durga@iitg.ac.in            \\
%%%% ---------------------------------- Professional Reference ----------------------------------------
%%{\bf Dr. Chinghua Chen-Ritzo}  \> {\bf Mr. Venugopal Subbarao}     \>  {\bf Mr. Suvendu Tripathy} \\
%% Research Director             \> Senior Principal Architect       \>   CEO \& Founder            \\
%% IBM Research Collaboratory    \> Infosys Labs	| SETLabs           \>   Mouse-e Retail Pvt Ltd    \\
%% IBM Singapore Pte Ltd         \> Infosys Technologies Ltd     	\>   Bangalore, India          \\
%% chinghua@sg.ibm.com           \> venugopal\_subbarao@infosys.com  \>   suvendu@mouse-e.com       \\
%%%% ---------------------------------- Professional Reference ----------------------------------------
%{\bf Dr. Chinghua Chen-Ritzo} \> {\bf Mr. Venugopal Subbarao}    \>  {\bf Ms. Donnie D. Hay}       \\
% Research Director            \> Senior Principal Architect 	 \>   Vice President - Analytics, \\
% IBM Research Collaboratory	  \> Infosys Labs			         \>   Solutions, and Acquisitions \\
% IBM Singapore Pte Ltd		  \> Infosys Technologies Ltd	     \>   IBM Corporation\\
% chinghua@sg.ibm.com 		  \> venugopal\_subbarao@infosys.com \>   dhaye@us.ibm.com            \\
%%%%%% --------------------------------- professional references ---------------------------------------
%%{\bf Dr. Chinghua Chen-Ritzo} \> {\bf Mr. Venugopal Subbarao}   \>  {\bf Mr. Rajesh Agarwal}   \\
%% Research Director            \> Senior Principal Architect 	 \>   WW Leader - Analytics COE \\
%% IBM Research Collaboratory	  \> Infosys Labs			     \>   IBM India Pvt Ltd         \\
%% IBM Singapore Pte Ltd		  \> Infosys Technologies Ltd	     \>   Bangalore, India          \\
%% chinghua@sg.ibm.com 		  \> venugopal\_subbarao@infosys.com \>   rajesh.agarwal@in.ibm.com \\
%%%%%% --------------------------------- professional references ---------------------------------------
%
%\end{tabbing}
%\end{small}

\smallbreak
%{sourced by: \textcolor{blue}{e}\textcolor{red}{X}\textcolor{blue}{ec}\textcolor{red}{Search}
%{\bf Sourced by: Puja Jain $\mid$ www.connectedgroup.com $\mid$ puja.jain@connectedgroup.com}
%\footnote{Balaji Vishwanathan}{www.ibanasia.com / info@ibanasia.com}
%\footnote{Tan Soon Kiat,
%10 Anson Road, \#26-04, International Plaza, Singapore 079903
%Tel : +65-63048926 Mobile: +65-96956191, www.xsearch.com.sg }}

\end{resume}
\end{document}

