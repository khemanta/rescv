% LaTeX Curriculum Vitae Template
%
% Copyright (C) 2004-2009 Kumar Hemant <khemanta@iitg.ac.in>
% http://jblevins.org/projects/cv-template/
%
% You may use use this document as a template to create your own CV
% and you may redistribute the source code freely. No attribution is
% required in any resulting documents. I do ask that you please leave
% this notice and the above URL in the source code if you choose to
% redistribute this file.
\RequirePackage{lineno}
\documentclass[letterpaper,10.0pt]{article} % Letter paper size
%\documentclass[a4paper,11.0pt]{article}    % A4 paper size
\usepackage{hyperref}
\usepackage{geometry}
%\usepackage{tfrupee}
% Comment the following lines to use the default Computer Modern font
% instead of the Palatino font provided by the mathpazo package.
% Remove the 'osf' bit if you don't like the old style figures.
\usepackage[T1]{fontenc}
\usepackage[sc,osf]{mathpazo}

\usepackage{graphicx}
\usepackage{graphics}
\usepackage{float,epsfig,floatflt,here}

\usepackage{amsmath}

% Set your name here
\def\name{Kumar Hemant}

% Replace this with a link to your CV if you like, or set it empty
% (as in \def\footerlink{}) to remove the link in the footer:
% \def\footerlink{http://jblevins.org/projects/cv-template/}
% \def\footerlink{http://researcher.ibm.com/view.php?person=sg-kumar.hemant}
\def\footerlink{http://researcher.watson.ibm.com/researcher/view.php?person=sg-kumar.hemant}
%\def\footerlink{Curriculum-Vitae}

% The following metadata will show up in the PDF properties
\hypersetup{
  colorlinks = true,
  urlcolor = black,
  pdfauthor = {\name},
  pdfkeywords = {computer science, mathematics, statistics, data mining, machine learning, computing, analytics},
  pdftitle = {\name: Curriculum Vitae},
  pdfsubject = {Curriculum Vitae},
  pdfpagemode = UseNone
}

\geometry{
% body={6.7in, 8.5in}, % for Letter  (8.50in X 11.00in)
  body={6.5in, 9.0in}, % for A4paper (8.26in X 11.69in)-(1.5inX1.5in)={6.57in,10.19in}
  left=1.0in,
  %top=1.15in  %for Letter
  top=1.35in   %for A4paper 
}

% 	Millimetres	Inches	 
% 	Width	Length	Width	Length
% a4Ppr	210.0	297.0	8.26	11.69
% usLtr	215.9	279.4	8.50	11.00

% Customize page headers
\pagestyle{myheadings}
\markright{\name}
\thispagestyle{empty}

% Custom section fonts
\usepackage{sectsty}
\sectionfont{\rmfamily\mdseries\Large}
\subsectionfont{\rmfamily\mdseries\itshape\large}

% Other possible font commands include:
% \ttfamily for teletype,
% \sffamily for sans serif,
% \bfseries for bold,
% \scshape for small caps,
% \normalsize, \large, \Large, \LARGE sizes.

% Don't indent paragraphs.
\setlength\parindent{0em}

% Make lists without bullets
\renewenvironment{itemize}{
  \begin{list}{}{
   \setlength{\leftmargin}{1.2em}
  }
}{
  \end{list}
}

\begin{document}
%\linenumbers{}
% Place name at left
{\huge \name}

% Alternatively, print name centered and bold:
%\centerline{\huge \bf \name}

\vspace{0.25in}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{-2.5cm}
\begin{figure}[!h]
%%\centering
\flushright
\psfig{file=147522.eps,width=.10\linewidth}
%%\psfig{file=NA_Hem.eps,width=.10\linewidth}
%%\hfill Kumar Hemant
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{minipage}{0.55\linewidth}
  %\href{http://www.iitg.ac.in//}{IIT Guwahati} \\
  %Faculty of Engineering \& Technology \\
  %Guwahati, India \\
  %INDIA
  Woodlands Drive 75 \\
  Blk 690B, \#07--174\\
  732690, Singapore
 % JP Nagar III Phase\\
 % Bangalore 560 078, India
\end{minipage}
\begin{minipage}{0.55\linewidth}
  \begin{tabular}{ll}
   % Phone: & (+91) (080) 4148 9185 \\
    Mobile: & (+65) 9457 7206 \\
    Email: & \href{mailto:k-hemant@hotmail.com}{k-hemant@hotmail.com} \\
    www: & \href{http://sg.linkedin.com/in/khemant}{sg.linkedin.com/in/khemant} \\
  \end{tabular}
\end{minipage}

%\medskip\hrule height 0.5pt % draws horizontal underline
%\section*{\sc Personal}
%\begin{itemize}
%\item Born on August 15, 1977.
%\item Indian Citizen.
%\end{itemize}

\section*{\sc Synopsis}
{ \it MS in Mathematics \& Computer Science from IITG with 5+ years of experience in Software Engineering, R\&D, IT/Consulting with focus on Data-Mining (Supervised, Semi-supervised \& Unsupervised), Machine Learning, Large Scale Scientific Computing, Business Analytics and Statistical Modeling.}

% $\sim$ or $ \textasciitilde $ or $ \texttildelow $ or $ \textbackslash $

%{\small MSc in applied mathematics \& computer science from IIT/G with 4+ years of experience in SAS programming, R\&D, IT and KPO with focus on Data-Mining, Machine Learning, Large Scale Computing, Software Engineering and Business Analytics in supporting clients in B2B space. Enriched experience of over 2 years in various statistical techniques (Linear Regression, Logistic Regression, Mixed Models, Survival Analysis, Customer Loyalty Analysis). Have experience in Financial Score Card model building for customer attrition, cross sell and up sell models and validation approach. Customer attrition model in online search and display advertisement.}

%\section*{\sc Synopsis}
%MSc in applied mathematics \& computer science from IITG with 4+ years of experience in SAS programming, Data Mining and Business Analytics (statistical modeling using linear and logistic regression, ad hoc reporting, etc.).% R\&D, IT and ITeS/KPO with focus on data-mining (supervised \& unsupervised), machine learning, software engineering and

%I am Certified Base SAS professional with overall 4.5 years of experience in data mining, operations research and data warehousing and supporting clients in Business-to-Business space. Experience of nearly 2 years to handle various statistical techniques like Linear Regression, Logistic Regression, Mixed Models, Survival analysis for Customer Loyalty Analysis, Distribution Theory and Reliability Lifetime Data Analysis.

%I had hands on experience on industry specific analysis like Price Elasticity Modeling \& Market Basket Analysis in retail and mixed models to study the cross sectional time series data. Financial Score Card model building for customer attrition, cross sell and up sell models and validation approach. Customer attrition model in online advertisement

%Exposure in domains like online search \& display advertisement, Retail, Airlines \& Aircraft manufacturing and basic financial concepts. Currently working on reliability statistics and engineering analysis to optimize the aircraft maintenance time intervals using the lifetime distributions.
\section*{\sc Education}
\begin{itemize}
%  \item {\bf Swiss Federal Institute of Technology (ETHZ) \&} \hfill Z\"urich \\
  \item {\bf Technion -- Iserael Institute of Technology ()}, Bangalore\hfill India\\
%  \item {\bf Technische Universiteit Einhdoven (TU/e)}, Eindhoven\hfill Holland
  \item {\bf Indian Institute of Science (IISc)}, Bangalore\hfill India\\
%  \item {\bf Indian Institute of Technology (IIT)}, Kanpur\hfill India \\
%  \item {\bf University of Zurich \& ETH Zurich } \hfill Zurich \\
%  \item {\bf University of Oxford - Sa\"id Business School} \hfill London \\
%  \item {\bf University of Cambridge - Judge Business School} \hfill London \\
%  \item {\bf Imperial College Business School} \hfill London \\
%  \item {\bf National University of Singapore (NUS)} \hfill Singapore \\
%  \item {\bf Universiteit Gent (UGent)}, Ghent \hfill Belgium \\
%  \item {\bf Ecol\'e Polytechnique (ParisTech)}, Paris \hfill France \\
%    	{ Ph.D., Computer Science} \hfill June'2016
%    	{ Joint M.Sc. UZH ETH, Quantitative Finance}  \hfill 2013--2014
     	{ M.Tech., Computer Scince \& Engineering} \hfill 2014--2016
%    	{ M.Tech., Computational Science} \hfill June'2016
%     	{ M.B.A., Analytics \& Strategy} \hfill 2013--2014
%      	{ M.M.A, Marketing Analytics} \hfill 2013--2014 
%	{ MS, Information Technology} \hfill 2013--2014 
    

  \item {\bf Indian Institute of Technology (IIT/G)}, Guwahati \hfill India \\
    { M.S., Mathematics \& Computer Science} \hfill June'2005
%    { M.Sc., Mathematics \& Computer Science} ({\sc Cgpa} 5.72) \hfill 2003--2005

  \item {\bf Banaras Hindu University (BHU)}, Varanasi \hfill India \\
    { B.S.,(Honors) Physics } \hfill June'2003
%    { B.Sc.,(Honours), Physics ({\sc Aggt.} 55.0\%)} \hfill 1999--2003

%  \item Patna Collegiate School, CBSE Board \hfill Patna \\
%    Std. X, Science,  ({\sc Aggt.} 76\%) \hfill ....
\end{itemize}

%\subsection*{Professional Education}
%\begin{itemize}
%\item {\bf Kelly School of Business--Indiana University-at-Bloomington}  \& Bloomington, USA
%\vspace{-0.3cm}
%\item {\bf Indian Institute of Management}	\&	Lucknow, India \\
%{\bf \em Certificate Programm in Business Analytics for Executives} - a joint professional programme \\
%\end{itemize}

%\begin{tabular}{|l|l|l|l|l|}
%  \hline
% % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
% % Year & Institute & University/Board & Degree & GPA/ \\
%  %&&&&\\
%		& 				& 					& Certificate \linebreak 	&    \\
%             	& Kelly School of Business	& Indiana University - Bloomington	& Program in \linebreak 	& A+ \\
%  2012--2013 	& IIM Lucknow  			& Indian Institute of Management 	& Business   \linebreak 	& A+ \\  
%  	     	& SAS R\&D Institute  		& SAS Institute  			& Analytics  \linebreak		& A+ \\
%		& 				& 					& for Enterpreneurs		&    \\	
% %  %&&&&\\
% % \hline
% \hline
%\end{tabular}

%\section*{Employment}
\section*{\sc Computing Skills}
\begin{itemize}
\item {\bf Languages :} C, SAS, SPSS, SQL, {\sc Java, Matlab}, MPI
\vspace{-0.2cm}
\item {\bf Scripting :} Python, Unix-shells, JavaScript
\vspace{-0.2cm}
\item {\bf O/S Platform :} Unix, Linux(Redhat, SuSE, Ubuntu), Solaris, WinXP/7
\vspace{-0.2cm}
\item {\bf Database :} MySQL, Informix, PostgreSQL, SciDB 
\vspace{-0.2cm}
\item {\bf Applications \& Text Processing :} \LaTeXe, MS Office, OpenOffice.Org
\vspace{-0.2cm}
\item {\bf Data Mining \& Machine Learning :} R, Weka, Mahout, Hadoop, $etc.$
\vspace{-0.2cm}
\item {\bf Scientific Data \& Applications :} ESRI/ArcGIS, NetCDF, HDF, GRIB, WRF, OpenFOAM, etc
\vspace{-0.2cm}
%\item {\bf Mathematical \& Statistical Analysis:} Notion of SAS/STAT, A bit of R \& Matlab
%\vspace{-0.2cm}
\item {\bf Quantitative Analysis :} Mathematical \& Statistical Modeling, Numerical \& Computer Simulations
\end{itemize}

%\section*{Internships}
%\begin{itemize}
% \item Juelich Supercomputing Center, Germany. \hfill ({\it End of Master thesis 4th Semester.})\\
%\end{itemize}

%\section*{\sc Relevant Courses}
%{\bf Applied Mathematics:} {\it Numerical Analysis \& Scientific Computing, Mathematical Modeling \& Numerical Simulation, Numerical Linear Algebra, Computational Fluid Dynamics, Optimization Techniques. } {\bf Computer Science:} {\it Programming in C, Data-Structure, Advance Algorithms, Graph Theory \& Combinatorics, Formal Languages \& Automata Theory, Theory of Computation etc.}

%\vfill

\section*{\sc Professional Experience}
\begin{itemize}

%\item {\bf Development Bank of Singapore Ltd} \hfill {\sc Singapore}.
%%\vspace{-.3cm}

%{\it Associate Vice President} \hfill {\it January, 2013 - July, 2014}\\
%%{\it AVP - Analytics \& eCommerce} \hfill {\it May, 2010 - Present}\\
%Business Analytics \& eCommerce Services (Technical Comptency) group DBS Singapore (DBS Research Lab).


\item {\bf IBM Singapore Pte Ltd} \hfill {\sc Singapore}.
%\vspace{-.3cm}

{\it Research Staff Software Engineer} \hfill {\it September, 2011 - Present}\\
%{\it Staff Software Engineer} \hfill {\it September, 2011 - December, 2012}\\
IBM Research Collab-Singapore, client-site located part of Business Analytics \& Mathematical Sciences (BAMS) group IBM Research (T J Watson Research Lab), the R\&D arm of { IBM Corp.}\\
%IBM Research Collaboratory - Singapore, part of IBM Singapore Pte Ltd.\\
$\circ$ Being part of the Business Analytics \& Mathematical Science (BAMS) group of IBM Research for IBM's Smarter Planet initiative specially Smarter Cities. Application using Large Scale Distributed Computing Architecture using MapReduce, Hadoop and Mahout (Large Scale Data Mining). My work involves design and implementation of API specification for access and query method of large scientific data (XML blobs, NetCDF, GRIB, etc).

$\circ$ Text Mining based application for semantic keyword match based Early Warning System for food poisonings and similar kind of events with streaming twitters and social network blog feeds. Further looking at anaphoric resolution of the flagged events and reduce false negative and false postive cases.

$\circ$ Worked with WRF and NOAA (National Oceanographic and Atmospheric Administration) simulated model tuned to be of higher spatial and temporal resolution (better than WRF models) to simulate and perform various data mining task to forecast more effectively and accurately. It involved bigdata (10's TB) from WRF/Wind Profiler/Radar Systems and other multi-modal sources. 


\item {\bf Infosys Technologies Ltd} \hfill Bangalore, INDIA.
%\vspace{-.3cm}

{\it Junior Research Associate / Member Research Staff} \hfill {\it May, 2010 - August, 2011}\\
%{\it Senior Associate Consultant/Senior Analyst} \hfill {\it May, 2010 - Present}\\
Software Engineering \& Technology Labs (Infosys Labs), the R\&D arm of {Infosys Technologies}, Bangalore, India.\\
$\circ$ Worked with the Analytics/BI product development group in capacity of Subject Matter Expert (SME) for development of a analytical model development workbench for configuring and customizing statistical and machine learning based model for telecom, banking and retail domain. The statistical techniques are regression (linear, logistics), supervised techniques (classification), un-supervised (clustering) and recommendation systems (collaborative filtering) etc.


\item {\bf Infosys Technologies Ltd} \hfill Bangalore, INDIA.
%\vspace{-.3cm}

{\it Analyst Specialist} \hfill {\it February, 2009 - April, 2010} \\
 On tenure-track secondment to SETLabs from Knowledge Services--Analytics unit. SETLabs, the R\&D unit of {Infosys Technologies}, Bangalore, India.\\
$\circ$ Worked in the area of customer analytics. Research aspect area includes text mining and machine learning techniques applicable in various domain using unstructured and structured data for a flagship product called Voice of Customer (VoC). %Consulting aspect of work involves ad hoc request for proposal and consultation for RFP, product landscaping, patent analysis. Liasoning  between solution team and engineering team for identifying the new and emerging area of analytics.


\item {\bf Infosys Technologies Ltd} \hfill Bangalore, INDIA.
%\vspace{-.3cm}

{\it Analyst Programmer} \hfill {\it August, 2007 - January, 2009}\\
At Knowledge Services--Analytics unit in close cooperation with Infosys Consulting, Inc; a wholly owned subsidiary of {Infosys Technologies}, Bangalore, India.\\
$\circ$ {\emph Client:} {\bf Microsoft Corporation.} Worked in {\it Analytics Center of Excellence (ACoE)} in capacity of SAS programmer and SME particularly in web-analytics area for MSN search and MSN Live (now bing.com). Developed generic codes to crunch the data using data-mining techniques to offer analytical insight and consultancy within web-analytics sphere. Made predictive and statistical model to help optimize the MSN ad placing, recommending automatic bidding strategy based on Dutch, English, GFP and GSP auction system.  Data used was click-stream keywords mapped search traffic data (from Omniture, ComScore, etc.).
%1.	{\bf Search \& Churn:} To figure out key customer (advertisers) who contribute to the largest basket of revenue using lift curve and pareto-principle. And pro-active retention of those who are prone to churn using churn model.\\
%2.	{\bf Capacity Forecast:} Optimal forecast of inventory (impressions) for advertiser at various page group level to reduce the under booking and over booking situation. Also to identify bidding strategy for the remnant and reserved ad space using automatic-bidding strategy based on Dutch and English auction system.\\
%3.	{\bf MSN Direct Response:} This project was to know using capacity forecast model, the opportunity cost of under-booking and over-booking along with performance of auction system of remnant ad space.\\
%4.	{\bf PPC Analysis:} Pay per click analysis was to look at the PPC as KPI over CPM, and to tap any potential fraud in clicks by ad-networks.


\item {\bf Few startups: Freelancing/Self-employed} \hfill Delhi/Pune, INDIA.
%\vspace{-.3cm}

{\it Technical Evangelist/Independent Consultant} \hfill {\it June, 2006 - July, 2007} \\
$\circ$ Worked with startup for entrepreneurial interest and collaborative assignments in the area of open source development for start-up software companies for integration of system \& web application and was part of development team in web hosted solution based on Joomla LAMP as CMS. %and testing phases of an in-house product call 'Think-T' for customized web based email hosting solution and spam guard. The product is based on LAMP configuration (Linux, Apache, MySQL and PHP). The open source code of Fedora Core 5 was taken into consideration while designing the product.


\item {\bf J\"ulich Supercomputing GmbH} \hfill Juelich, GERMANY.
%\vspace{-.3cm}

{\it Wissenschaftlicher-Mitarbeiter/Scientist} \hfill {\it October, 2005 - May, 2006}\\
$\circ$ Worked as young scientist cum scientific programmer on IBM Bluegen supercomputer cluster at J\"ulich Supercomputing Center of Helmholtz Research Center Juelich GmbH, Germany.% on serial and parallel open source code of Fire Dynamics in Computational Fluid Dynamics (CFD).

 1.	Worked on open source code of FDS in the area of CFD in serial and parallel environment. Wrote executables for data processing and analysis using C, MPI routines and UNIX Shell-scripting. \\
    %Wrote a technical paper "FDS: A User Interface for Simulation of FDS Version 4.0" for Unix/Linux platform. Designed input files for simulation of FDS (Fire Dynamics simulator). Performed simulations and analysis of the results for different run and test cases of simulation for serial and parallel environment.
    %Pedestrian dynamics and simulation of traffic flow of human behavior during stampede. simulation of pedestrian movement and dynamic lane formation.
 2. Participated and collaborated with Fire and Safety engineering group of University of Wuppertal. Part of the work was funded by DFG for traffic simulation in stadia/arena to test the safety measure of the stadium in likelihood of a stampede for the FIFA World Cup in Germany in Year 2006.
\end{itemize}

\section*{\sc Accolades}
\vspace{-0.2cm}
%\begin{itemize}
{\it %DAAD-Helmholtz young scientist studium (fellowship), 2005 \\
%German Research Foundation (DFG) research studium (fellowship), 2006 \\
%Secured highest percentile in a national level GATE'05 (Engineering Science) in IITG zone\\
%Merit-cum-means scholarship at IIT Guwahati during master course work \\
$\bullet$ Studium (fellowship) for research at Helmholtz Research Center Juelich, Germany. \\
$\bullet$ Qualified national level Graduate Aptitude Test in Engineering (GATE 2005) in Engineering Science (XE) with highest percentile in IIT-G zone. \\
$\bullet$ Won in `On the Spot Programming Contest' in Techniche 2005 at IIT Guwahati. \\
}
%\end{itemize}

\section*{\sc Patents}
\begin{itemize}
 \item ``{\sc Method and System for Contextual Advertisement Recommendation Across Multiple Devices of Content Delivery.}" ({\it USPTO 2012/0078,725})
 %\item	{\it Method and Systems for online auction systems for stability against gaming-the-system.} - {disclosed - filing under progress}
 %\item	{\it Optimizing the inventory and replenishment with Real-Time-Information flow  using the SKUs code.} - {disclosed - filing under progress}
\\
{\bf PATENTS UNDERFILING}
 \item	{Method And Systems For Online Auction Systems for Stability Against Gaming-The-System.} {:: \it Disclosed - filing under process.}
  \vspace{-0.1cm}
 \item	{Optimizing The Inventory And Replenishment With Real-Time-Information Flow  Using The SKU Code.} {:: \it Disclosed - filing under process.}
  \vspace{-0.1cm}
\end{itemize}

\section*{\sc Technical/Research Papers}
\begin{enumerate}
 \item Kumar Hemant, Vikas Gupta, Abhijit Choudhary (Dec 2004): {\it ``Numerical Solution of 2D Navier-Stokes Equation for Incompressible Viscous Lid-Driven Cavity Flow"}, Department of Mathematics, IIT Guwahati.
 \item Manish Mishra, Chandan Pandey, Kumar Hemant (Jul 2005): {\it ``Assembly of Stiffness Matrix in Shared Memory Environment"}, Department of Mechanical Engineering, IIT Guwahati.
 \item Kumar Hemant, Bernd Koerfgen, Armin Seyfried, (Dec 2005): {\it ``FDS: A User Interface for Simulation on Unix/Linux Platform"}, ZAM, Forschungszentrum Juelich, Germany.
 \item Armin Seyfried, Kumar Hemant, et al, (April 2006): {\it``Simulation of Pedestrian Dynamics with Bottle-Neck Effect"}, ZAM, Research Center Juelich, Germany
 \item Kumar Hemant, Petra Jansen, Dora Erd\"os, et al.,(Apr 2009): {\it ``Optimization routine for hot rolling mill"}, IJmuiden Research Lab, Corus Netherlands B.V., The Netherlands.
\end{enumerate}

\section*{\sc Workshops/Conferences}
\begin{enumerate}
 \item Mathematical Training \& Talent Search (MTTS) held at IIT Guwahati (Dec 2004)
 \item NBHM and INSA Summer School on Stochastic Process \& Applications held at IIT Guwahati (11th - 23rd July, 2005)
 \item Mathematical Modeling Week at Technical University of Eindhoven, The Netherlands (28th March 2008 - 11th April 2008)
\end{enumerate}

%\section*{\sc Patents Filed}
%\begin{itemize}
% \item ``{\sc Method and System for contextual advertisement recommendation across multiple devices of content delivery.}" ({\it :Patent filed in IPTO and USPTO.})
%% \item {\it Method and Systems for online auction systems for stability against gaming-the-system.} - {disclosed - filing under progress}
%% \item {\it Optimizing the inventory and replenishment with Real-Time-Information flow  using the SKUs code.} - {disclosed - filing under progress}
%\end{itemize}

\section*{\sc Professional Certifications @}
\vspace{-0.2cm}
From the College Of Engineering Pune (COEP) \verb"<www.coep.org.in>"
\vspace{-0.2cm}
\begin{itemize}
  \item Certificate course in Linux \& Networking at COEP, Pune.
\vspace{-0.2cm}
  \item Certificate course in Software Testing from COEP, Pune.
\vspace{-0.2cm}
 % \item Certified Software Quality Test Analyst from Computer Society of India, Chennai.
%\vspace{-0.2cm}
 % \item Certified Base SAS Programmer from SAS Institute, Inc., North-Carolina, USA.
%\vspace{-0.2cm}
%  \item Infosys internal trainings and certifications on SAS, SQL, Python, Perl, etc.
\end{itemize}

%\section{\sc Relevant\\Courses}
%{\bf Applied Mathematics : } {\it Numerical Analysis \& Scientific Computing, Mathematical Modeling \& Numerical Simulation, Numerical Linear Algebra, Computational Fluid Dynamics, Optimization Techniques} {\bf Computer Science : } {\it Programming in C, Data-Structure, Algorithms, Graph Theory \& Combinatorics, Formal Languages \& Automata Theory etc.}

\section*{\sc Cocurricular Activities}
\vspace{-0.2cm}
\begin{enumerate}
 \item Secretary, Mountaineering \& Trekking Club at Gymkhana Council, IIT Guwahati.
\vspace{-0.2cm}
 \item Certificate in Photography from Hobby Center, BHU Varanasi.
\vspace{-0.2cm}
 \item Certificate in Mountaineer training course from NIM, Uttarkashi.
\vspace{-0.2cm}
 \item Climbed a peak in the Himalayas as rope leader in annual mountaineering expedition year 2001 in the vicinity of Adi Kailash (\~ 18000ft), Pithoragarh in Uttaranchal.
\vspace{-0.2cm}
 \item Represented Faculty of Science in the university level events of Quiz, Debate \& GD in the Inter-Faculty Youth Festival held at IT-BHU in year 2003.
\vspace{-0.2cm}
 %\item Organizing member of AKANKSHA, a university level cultural festival of BHU Varanasi.
 \item Served as student secy. for BHU Chapter of Indian Association of Physics Teachers.
\vspace{-0.2cm}
 \item Attended a Diploma in French course at Dept of French Studies, BHU Varanasi.
\vspace{-0.2cm}
 \item Attended basic course in German language at Gastehaus Forschungszentrum Juelich, Germany.
\end{enumerate}

 \section*{\sc Relevant Courses}
 $\bullet$ {\bf Applied Mathematics:} {\it Real Analysis, Complex Analysis, Linear Algebra, Ordinary \& Partial Differential Equations, Abstract Algebra, Mathematical Statistics \& Probability Theory, Random Variables \& Stochastic Process, etc. } $\bullet$ {\bf Computational Mathematics:} {\it Numerical Analysis \& Scientific Computing, Mathematical Modeling \& Numerical Simulation, Numerical Linear Algebra, Computational Fluid Dynamics, Linear Programming \& Optimization Techniques, etc. } $\bullet$ {\bf Computer Science:} {\it Programming in C, Data-Structure, Algorithms, Graph Theory \& Combinatorics, Formal Languages \& Automata Theory, Theory of Computation, etc.} $\bullet$


\section*{\sc Personal Details}
%\vspace{-0.05cm}
\begin{tabbing}
%1234567890123 \= 12345678901234567890123456\= 123456789012345678901234567 \= 123456789012345678901234567  \kill
 12345678901234567 \= 123456\= \kill
 Father's Name   \> : \> Late Sri D. N. Mishra \\
 DOB             \> : \> August 15, 1977 \\
 Nationality     \> : \> Indian \\
 Marital Status  \> : \> Married \\
 Passport        \> : \> F3211761   \\
 Visa Status     \> : \> B1/B2 (Multiple entry business visa for US)\\
 Working Visa	 \> : \> Singapore 
\end{tabbing}

%\section*{\sc Additional Information}
%%\vspace{-0.05cm}
%\begin{tabbing}
% 12345678901234567 \= 123456\= \kill
%Current CTC	 \> :\> \rupee 782,400.00/annum \\
%Current CTC	 \> :\> {\sc SG\$} 6,300.00/month \\
%Expected CTC	 \> :\> {\sc US\$} 8,000.00/month {\it (Negotiable)}.\\
%Notice Period	 \> :\> 4--6 weeks \\
%Total Exp.	 \> :\> 6.0 Years \\
%Relevant Exp.	 \> :\> 5.5 Years \\
%Current Location \> :\> Singapore \\
%Relocation	 \> :\> Preferably US/UK/EU/ANZ/ASEAN regions.\\
%		 \> :\> Open for good opportunities and startups in India (BLR/NCR/HYD/PUNE) and onsites.\\ 
%%Special Mention\>:\> Out-of-box impromptu thinking.
%%Personal Photo	\>:\>
%\end{tabbing}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section*{Curricular}
%
%\subsection*{Co-curricular}
%\begin{itemize}
% \item {\bf Academic:} Ankit Fadia Certified Ethical Hacker (AFCEH), Batch-2, Version-3.0
% \item {\bf Sports:} Cricket, Badminton, Table Tennis
% \item {\bf Cultural:}  Stage performance and Singing songs.
%\end{itemize}
%
%\subsection*{Extra-curricular}
%\begin{itemize}
% \item Active participant as an organizer of Academic Festival "ICKNIGHTED 2007"
% \item Active participant as an organizer of Sports Festival "ICTHALON 2005 \& 2006"
%\end{itemize}
%
%\subsection*{Soft-skills and Strength}
%\begin{itemize}
% \item Punctuality
% \item Hard working
% \item Confident
% \item Very friendly nature
% \item Strong communication skills
%\end{itemize}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{\sc References}
% \vspace{-0.17in}
 \begin{tabbing}
   123456789012345678901234567890123 \= 1234567890123456789012345678901234567 \= \kill
   %1234567890123456789012345678901234567 \= \= \kill
%   Dr Ruediger Esser                	\> \>   	Jayant R Kalagnanam  \\
%   Dy Director, ZAM                 	\> \>   	Founder Research Director \\
%   Forschungszentrum Juelich GmbH   	\> \>   	IBM T J Watson Research Center \\
%   Research Center Juelich, Germany 	\> \>   	IBM Corporation \\
%   r.esser@fz-juelich.de 		\> \> 		jayant@us.ibm.com\\ \\

{\bf Dr Ching-Hua Chen-Ritzo}   \> {\bf Dr Jun Zhang}    	\>  {\bf Mr. Suvendu Tripathy} \\
 Research Director              \> Research Scientist 	  	\>   CEO \& Founder \\
 IBM Research Collaboratory	\> IBM Research Collaboratory	\>   Mouse-e Retail Pvt Ltd \\
 IBM Singapore Pte Ltd		\> IBM Singapore Pte Ltd  	\>   Bangalore, India \\
 chinghua@sg.ibm.com 		\> zhangjun@sg.ibm.com 		\>   suvendu@mouse-e.com\\
 \end{tabbing}


\bigskip
% Footer
\begin{center}
  \begin{footnotesize}
 %   Last updated: \today \\
    Last updated: September 30, 2012 \\
    \href{\footerlink}{\texttt{\footerlink}}
  \end{footnotesize}
\end{center}

\end{document}
